class TeamXFan implements Observer<Game> {
  update(game: Game): void {
    if (game.name.includes('Team X')) {
      console.log('Yay', game);
    }
  }
}
