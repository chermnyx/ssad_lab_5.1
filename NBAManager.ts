class NBAManager {
  publisher: NBA;
  constructor() {
    this.publisher = new NBA();
  }
  addGame() {
    this.publisher.notify({
      date: new Date(),
      name: 'Team X vs Team Y',
      score: [1, 3],
    });
  }

  subscribeToGame(obs: Observer<Game>) {
    this.publisher.subscribe(obs);
  }
}
