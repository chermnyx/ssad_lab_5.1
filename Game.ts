type Game = {
  name: string;
  date: Date;
  score: [number, number];
  members?: string[];
};
