class Publisher<T> implements IPublisher<T> {
  private observers: Observer<T>[];

  subscribe(observer: Observer<T>): void {
    this.observers.push(observer);
  }

  unsubscribe(observer: Observer<T>): void {
    this.observers = this.observers.filter((x) => x != observer);
  }

  notify(data: T) {
    this.observers.forEach((obs) => obs.update(data));
  }
}
